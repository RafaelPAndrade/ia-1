#!/usr/bin/env python3
# IA 2018/19, Projeto 1, Grupo 14
# Miguel Cordeiro Francisco, Rafael Pestana de Andrade

from search import Problem

# ______________________________________________________________________________
# (Given) Abstract data types [TAI]


# TAI content
def c_peg():
    return "O"
def c_empty():
    return "_"
def c_blocked():
    return "X"
def is_empty(e):
    return e == c_empty()
def is_peg(e):
    return e == c_peg()
def is_blocked(e):
    return e == c_blocked()


# TAI pos
# Tuplo (l, c)
def make_pos(l, c):
    return (l, c)
def pos_l(pos):
    return pos[0]
def pos_c(pos):
    return pos[1]

# TAI move
# Lista [p_initial, p_final]
def make_move(i, f):
    return [i, f]
def move_initial(move):
    return move[0]
def move_final(move):
    return move[1]

# ______________________________________________________________________________
# Board manipulation functions


def board_moves(board):
    res = []
    max_x = len(board) - 1
    max_y = len(board[0]) - 1

    for x_row in range(max_x + 1):
        for y_collumn in range(max_y + 1):
            if not is_peg(board[x_row][y_collumn]):
                continue

            if 0 < x_row < max_x:
                top_p    = board[x_row - 1][y_collumn]
                bottom_p = board[x_row + 1][y_collumn]

                offset = 0
                if is_peg(top_p) and is_empty(bottom_p):
                    offset = -1
                elif is_empty(top_p) and is_peg(bottom_p):
                        offset = 1

                if offset:
                    res.append(make_move(make_pos(x_row + offset, y_collumn),
                                         make_pos(x_row - offset, y_collumn)))


            if 0 < y_collumn < max_y:
                left_p  = board[x_row][y_collumn - 1]
                right_p = board[x_row][y_collumn + 1]

                offset = 0
                if is_peg(left_p) and is_empty(right_p):
                        offset = -1
                elif is_empty(left_p) and is_peg(right_p):
                        offset = 1

                if offset:
                    res.append(make_move(make_pos(x_row, y_collumn + offset),
                                         make_pos(x_row, y_collumn - offset)))

    return res


def board_perform_move(board, move):
    res = [row[:] for row in board]

    pos1 = move_initial(move)
    pos1_x = pos_l(pos1)
    pos1_y = pos_c(pos1)

    pos2 = move_final(move)
    pos2_x = pos_l(pos2)
    pos2_y = pos_c(pos2)

    middle_x = (pos1_x + pos2_x) >> 1
    middle_y = (pos1_y + pos2_y) >> 1

    res[pos1_x][pos1_y] = c_empty()
    res[middle_x][middle_y] = c_empty()
    res[pos2_x][pos2_y] = c_peg()
    return res


def count_isopegs(board):
    isopegs = 0
    offset = 0
    max_x = len(board) - 1
    max_y = len(board[0]) - 1


    for x_row in range(max_x + 1):
        for y_collumn in range(max_y + 1):
            if not is_peg(board[x_row][y_collumn]):
                continue

            if 0 < x_row < max_x:
                top_p  = board[x_row - 1][y_collumn]
                bottom_p = board[x_row + 1][y_collumn]

                if(is_empty(top_p) and is_empty(bottom_p)):
                    if (y_collumn == 0 and is_empty(board[x_row][y_collumn+1])) or \
                        (y_collumn == max_y and is_empty(board[x_row][y_collumn-1])):
                        isopegs+=1
                        offset = 0
                        continue
                    else:
                        offset +=1

            if y_collumn > 0 and y_collumn < max_y:
                left_p  = board[x_row][y_collumn - 1]
                right_p = board[x_row][y_collumn + 1]
                if is_empty(left_p) and is_empty(right_p):
                    if((x_row == 0 and is_empty(board[x_row + 1][y_collumn])) or (x_row == max_x and is_empty(board[x_row-1][y_collumn]))):
                        isopegs+=1
                        offset = 0
                        continue
                    else:
                        offset+=1

            if(offset == 2):
                isopegs+=1

            offset = 0
    return isopegs




def update_isopegs(board, move, isopegs):
    max_x = len(board) - 1
    max_y = len(board[0]) - 1

    initial_pos = move[0]
    final_pos = move[1]
    eaten_pos = ((initial_pos[0]+final_pos[0])>>1, (initial_pos[1]+final_pos[1])>>1)


    affected_pos = [(initial_pos[0], initial_pos[1] - 1), (initial_pos[0], initial_pos[1] + 1),
                    (initial_pos[0] - 1, initial_pos[1]), (initial_pos[0] + 1, initial_pos[1]),
                    (eaten_pos[0], eaten_pos[1] - 1), (eaten_pos[0], eaten_pos[1] + 1),
                    (eaten_pos[0] - 1, eaten_pos[1]), (eaten_pos[0] + 1, eaten_pos[1])]


    possible_isopegs = [(final_pos[0], final_pos[1] - 1), (final_pos[0], final_pos[1] + 1),
                        (final_pos[0] - 1, final_pos[1]), (final_pos[0] + 1, final_pos[1])]


    for p in affected_pos:
        if 0 <= p[0] <= max_x and 0 <= p[1] <= max_y:
            if is_peg(board[p[0]][p[1]]):
                if is_isopeg(board, p):
                    isopegs += 1

    for p in possible_isopegs:
        if 0 <= pos_l(p) <= max_x and 0 <= pos_c(p) <= max_y:
            if is_peg(board[p[0]][p[1]]):
                if was_isopeg(board, p, final_pos):
                    isopegs -= 1
    return isopegs



def was_isopeg(board, pos, changed_pos):
    x = pos_l(pos)
    y = pos_c(pos)
    max_x = len(board) - 1
    max_y = len(board[0]) - 1
    offset = 0

    affected_pos = [make_pos(x, y-1), make_pos(x, y+1), make_pos(x-1, y), make_pos(x+1, y)]

    for p in affected_pos:
        if 0 <= pos_l(p) <= max_x and 0 <= pos_c(p) <= max_y:
            if p == changed_pos:
                offset += 1

            elif is_peg(board[pos_l(p)][pos_c(p)]):
                offset += 1


    return offset == 1


def is_isopeg(board, pos):
    x = pos_l(pos)
    y = pos_c(pos)
    max_x = len(board) - 1
    max_y = len(board[0]) - 1
    offset = 0

    if 0 < x < max_x:
        top_p = board[x - 1][y]
        bottom_p = board[x + 1][y]
        if is_empty(top_p) and is_empty(bottom_p):
            if (y == 0 and is_empty(board[x][y+1])) or (y == max_y and is_empty(board[x][y-1])):
                return 1
            else:
                offset += 1

    if 0 < y < max_y:
        left_p = board[x][y - 1]
        right_p = board[x][y + 1]
        if is_empty(left_p) and is_empty(right_p):
            if (x == 0 and is_empty(board[x + 1][y])) or (x == max_x and is_empty(board[x-1][y])):
                return 1
            else:
                offset += 1

    return offset == 2
# ______________________________________________________________________________
# Problem modelation


class sol_state:

    def __init__(self, board, n_pegs=False):


        def get_board_size(board):
            observed_min_x = len(board)
            observed_max_x = -1
            observed_min_y = len(board[0])
            observed_max_y = -1

            for x_row in range(0, len(board)):
                for y_collumn in range(0, len(board[0])):
                    if not is_peg(board[x_row][y_collumn]):
                        continue

                    observed_min_x = min(observed_min_x, x_row)
                    observed_max_x = max(observed_max_x, x_row)
                    observed_min_y = min(observed_min_y, y_collumn)
                    observed_max_y = max(observed_max_y, y_collumn)

            return [observed_min_x, observed_max_x, observed_min_y, observed_max_y]

        def count_pegs(board):
            return len([pos for row in board for pos in row if is_peg(pos)])


        self.board = board
        self.pegs = (n_pegs or count_pegs(board))
        new_max_min = get_board_size(board)

        self.min_x = new_max_min[0]
        self.max_x = new_max_min[1]
        self.min_y = new_max_min[2]
        self.max_y = new_max_min[3]

    def __lt__(self, other_sol_state):
        return (len(board_moves(self.board)) >
                len(board_moves(other_sol_state.board)))

    def apply_move(self, move):
        self.isopegs = update_isopegs(self.board, move, self.isopegs)
        return self.__class__(board_perform_move(self.board, move),
                              self.pegs - 1)

    def get_board_moves(self):
        return board_moves(self.board)

class solitaire(Problem):

    def __init__(self, board):
        super().__init__(sol_state(board))

    def actions(self, state):
        return state.get_board_moves()

    def result(self, state, action):
        return state.apply_move(action)

    def goal_test(self, state):
        return state.pegs == 1

    def path_cost(self, c, state1, action, state2):
        return c+1

    def h(self, node):
        return node.state.pegs + \
                (node.state.max_x - node.state.min_x)*(node.state.max_y - node.state.min_y)
