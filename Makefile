# Main Makefile
# IA 2018/19
# Rafael Andrade, Miguel Francisco
# P1

SHELL         := /bin/sh
RELEASE_NAME  := IA1_14


SRC_FILES   := main.py
TST_DIR     := tests
TST_ZIP     := IA201819-Solitaire-Tests

TST_COMM    := ./test.sh

ZIP         := zip
UNZIP       := unzip -qo
ZIP_FLAGS   := -ur --quiet


REPORT_NAME  := docs/relatorio-al014
.DEFAULT_GOAL := .DEFAULT

.DEFAULT:
	$(MAKE) $(MAKECMDGOALS) -C $(OBJ_FILES)

.PHONY : zip test do cleantest report analysis
zip :
	$(ZIP) $(ZIP_FLAGS) $(RELEASE_NAME).zip $(MAKEFILE_LIST) ||:
	$(ZIP) $(ZIP_FLAGS) $(RELEASE_NAME).zip $(SRC_FILES) ||:


test :
	cd $(TST_DIR) && $(UNZIP) $(TST_ZIP).zip
	cd $(TST_DIR) && $(TST_COMM)

do :
	$(MAKE) -C $(OBJ_FILES)

report :
	pandoc -f markdown -t latex $(REPORT_NAME).md -o $(REPORT_NAME).pdf

