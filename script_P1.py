#!/usr/bin/env python3
from main import *
from search import *
import time


dfs = True
greedy = True
astar = True
rep = 10

board_list = []
boards = ["5x5", "4x4", "4x5", "4x6"]


DFStimeValues = {}
AstartimeValues = {}
GreedytimeValues = {}

for board in boards:
    DFStimeValues[board] = []
    AstartimeValues[board] = []
    GreedytimeValues[board] = []


board = [["_","O","O","O","_"],["O","_","O","_","O"],["_","O","_","O","_"],["O","_","O","_","_"],["_","O","_","_","_"]]
board_list += [board]
board = [["O","O","O","X"],["O","O","O","O"],["O","_","O","O"],["O","O","O","O"]]
board_list += [board]
board = [["O","O","O","X","X"],["O","O","O","O","O"],["O","_","O","_","O"],["O","O","O","O","O"]]
board_list += [board]
board = [["O","O","O","X","X","X"],["O","_","O","O","O","O"],["O","O","O","O","O","O"],["O","O","O","O","O","O"]]
board_list += [board]


def one_run(board, board_size):
    print("Board {}".format(board_size))

    for i in range(rep):
        if dfs:
            print("DFS -    ", end="")
            game = solitaire(board)
            P = InstrumentedProblem(game)
            start = time.time()
            result = depth_first_tree_search(P)
            end = time.time()
            time_total = end - start
            print("time: {}\t|succs: {}\t|goal_tests: {}\t|states: {}".format(time_total,
                    P.succs, P.goal_tests, P.states))

            DFStimeValues[board_size] += [end-start]

        if greedy:
            print("Greedy - ", end="")
            game = solitaire(board)
            P = InstrumentedProblem(game)
            start = time.time()
            result = greedy_best_first_graph_search(P, P.h)
            end = time.time()
            time_total = end - start
            print("time: {}\t|succs: {}\t|goal_tests: {}\t|states: {}".format(time_total,
                    P.succs, P.goal_tests, P.states))

            GreedytimeValues[board_size] += [end-start]


        if astar:
            print("A* -     ", end="")
            game = solitaire(board)
            P = InstrumentedProblem(game)
            start = time.time()
            result = astar_search(P)
            end = time.time()
            time_total = end - start
            print("time: {}\t|succs: {}\t|goal_tests: {}\t|states: {}".format(time_total,
                    P.succs, P.goal_tests, P.states))

            AstartimeValues[board_size] += [end-start]

    show_average_time(board_size)

def show_average_time(board_size):
    if dfs:
        print("DFS: ",
                sum(DFStimeValues[board_size])/len(DFStimeValues[board_size]))
    if greedy:
        print("Greedy: ",
                sum(GreedytimeValues[board_size])/len(GreedytimeValues[board_size]))
    if astar:
        print("Astar: ",
                sum(AstartimeValues[board_size])/len(AstartimeValues[board_size]))



try:
    for i in range(len(board_list)):
        one_run(board_list[i], boards[i])
except KeyboardInterrupt:
    print()
