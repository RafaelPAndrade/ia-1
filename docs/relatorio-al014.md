---
title: Relatório do projeto 1 de IA - grupo AL014 - Peg Solitaire
documentclass: article
author:
- Miguel Francisco, n. 88080
- Rafael Andrade, n. 86503
papersize: A4
fontsize: 14pt
geometry: vmargin=1.5cm
geometry: margin=2.5cm
---

## Análise Prática

Os resultados obtidos para cada um dos problemas fornecidos foram os seguintes[^nota1]:

Legenda das tabelas:

 - P: Fator de Penetrância
 - b\*: Fator de Ramificação Efetiva

### - Tabuleiro 5x5 (Número de peças: 10, tamanho do espaço de estados: 167)

|        Tipo de procura     |   Tempo (s) |   Nós expandidos   |   Nós gerados   |  P      | b\*     |
|:--------------------------:|:------------|-------------------:|----------------:|:--------|:--------|
| DFS                        | 0.00234     | 13                 | 21              | 0.47619 | 1.16639 |
| Greedy                     | 0.00307     | 10                 | 19              | 0.52632 | 1.14691 |
| A\*                        | 0.01454     | 22                 | 44              | 0.22727 | 1.31096 |


### - Tabuleiro 4x4 (Número de peças: 14, tamanho do espaço de estados: 113734)

|        Tipo de procura     |   Tempo (s) |   Nós expandidos   |   Nós gerados   |  P      | b\*     |
|:--------------------------:|:------------|-------------------:|----------------:|:--------|:--------|
| DFS                        | 0.67263     | 5984               | 6002            | 0.00217 | 1.83831 |
| Greedy                     | 0.01742     | 38                 | 74              | 0.17568 | 1.23082 |
| A\*                        | 1.11957     | 1227               | 1679            | 0.00774 | 1.64796 |


### - Tabuleiro 4x5 (Número de peças: 16, tamanho do espaço de estados: 237591)

|        Tipo de procura     |   Tempo (s) |   Nós expandidos   |   Nós gerados   |  P      | b\*     |
|:--------------------------:|:------------|-------------------:|----------------:|:--------|:--------|
| DFS                        | 9.27195     | 80497              | 80527           | 0.00019 | 2.02962 |
| Greedy                     | 2.15909     | 12517              | 12563           | 0.00119 | 1.77539 |
| A\*                        | 0.07814     | 42                 | 153             | 0.10458 | 1.26187 |


### - Tabuleiro 4x6 (Número de peças: 20, tamanho do espaço de estados: superior a 1141711)


|        Tipo de procura     |   Tempo (s) |   Nós expandidos   |   Nós gerados   |  P      | b\*     |
|:--------------------------:|:------------|-------------------:|----------------:|:--------|:--------|
| DFS                        | 3.47957     | 20025              | 20083           | 0.00050 | 1.59971 |
| Greedy                     | 0.05404     | 19                 | 126             | 0.15079 | 1.16803 |
| A\*                        | 0.27081     | 57                 | 309             | 0.06149 | 1.24171 |



[^nota1]: Os testes foram executados nos servidores do sistema Sigma da DSI, correndo num computador com processadores Intel(R) Xeon(R) CPU E5-2620 @ 2.00GHz, 8-core, com 16GB de RAM.


\newpage


## Análise dos resultados obtidos

Em primeiro lugar, notamos que a complexidade de resolução de um tabuleiro não está diretamente relacionada com o seu tamanho -- o tabuleiro 5x5 é o mais rápido de resolver -- mas sim com o tamanho do espaço de estados do problema -- no tabuleiro 5x5 existem apenas 167 estados possíveis, comparado com os 237591 estados possíveis no espaço de estados do tabuleiro 4x5.

Por outro lado, o problema _peg solitaire_ tem três características interessantes: o espaço de estados é uma árvore, o custo de um estado é igual à sua profundidade na árvore e a solução está sempre no mesmo nível de profundidade na árvore de pesquisa, que é o número de peças inicial menos 1. A primeira característica advém do facto que não é possível adicionar novas peças durante o jogo (todas as jogadas permitidas removem sempre uma peça); a segunda, porque o custo dos sucessores de um estado é sempre mais um do que o custo do estado que os gerou; e a terceira característica é consequência de que, para resolver um tabuleiro, é preciso sempre fazer o mesmo número de jogadas.

A heurística utilizada para as procuras informadas foi a soma entre o número de peças presentes no tabuleiro e a dimensão do menor quadrado envolvente. O menor quadrado envolvente é definido pelos valores extremos de linha e coluna com peças. Por exemplo, um tabuleiro que só tenha duas peças, uma na linha 1 e coluna 2 e outra peça na linha 4 e coluna 3 tem como dimensão do menor quadrado envolvente $(4-1)\times(3-2) = 3\times1 = 3$.

Esta heurística não parece ser admissível, uma vez que é possível ter um quadrado envolvente bastante grande mas o custo de atingir um estado objetivo ser pequeno. Por exemplo, para resolver o seguinte tabuleiro são necessáras apenas três jogadas mas o valor da função heurística é $4 + (3-1)\times(3-1) = 8$:


    [["_", "O", "O"],
     ["O", "_", "_"],
     ["_", "O", "_"]]


Apesar da não admissibilidade, a heurística é eficiente, como se pode verificar pelos tempos reduzidos de procura, especialmente nos tabuleiros mais complexos, onde é possível observar reduções de tempo bastante significativas, na ordem das centenas[^nota2]  (veja-se o tempo da DFS vs A\* no tabuleiro 4x5).

Analisando a função de avaliação, especialmente no caso da procura A\*, reparámos no facto de que a função de custo é cancelada pela função heurística, uma vez que o custo da solução, que é o número de peças capturadas, somado com o número de peças presentes no tabuleiro dá um valor constante, que é o número de peças inicial. Portanto, podemos concluir que a procura A\* comporta-se como uma procura gananciosa, em que a função heurística é apenas a dimensão do menor quadrado envolvente.

Seria expectável que a procura gananciosa fosse a melhor procura informada para estes problemas, uma vez que esta procura priveligia o custo da procura em vez do custo da solução, e como já foi escrito acima, todas as soluções têm o mesmo custo para o mesmo problema -- e também é por causa disto que esta procura não garantir optimalidade da solução não é um problema, porque qualquer solução é óptima. Esta expectativa é confirmada pelos resultados obtidos -- a procura gananciosa foi a mais rápida em dois dos quatro problemas apresentados, e a melhor procura informada em três.


Nos tabuleiros menos complexos, a DFS mostrou-se uma boa opção (vide tabuleiro 5x5). Nós pensamos que o seu bom desempenho está ligado às (já referidas) características do problema: como o espaço de estados é uma árvore, não existe o perigo de entrar num ciclo infinito; e como todos os estados objetivo se encontram à mesma profundidade, as procuras em profundidade serão sempre mais eficientes comparadas com procuras de custo uniforme, que neste problema degenerariam em procuras em largura primeiro; e as procuras em profundidade são melhores (neste problema) que procuras em largura primeiro, pois estas últimas gastariam muito tempo a processar todos os estados a profundidades anteriores à da solução.





[^nota2]: A procura DFS apresenta valores anormalmente baixos para estes problemas. Isto deve-se à nossa implementação da função sol_state.\_\_lt\_\_ , que compara estados do tabuleiro através do número de jogadas possíveis. Outras definições poderiam ser utilizadas, como o número de peças em cada tabuleiro, que dariam valores dentro do esperado, isto é, cerca de 30 minutos no tabuleiro 4x6.
