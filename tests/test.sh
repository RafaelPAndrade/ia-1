#!/bin/sh -e

# Assumptions:
#   - This test runs in a Unix/POSIX-compatible machine, with at least:
#       + sh (tested with dash)
#       + coreutils
#       + xargs (findutils)
#   - The default configuration assumes that awk is somewhere installed in
#     $PATH, but that can be changed (use shell_aggregator)
#   - The test files:
#       + Are contained in a subdirectory, by default "students-tests"
#       + Each test consists of exactly 2 files:
#           * XXXXX${INPUT_SUFFIX}, the input
#           * XXXXX${OUTPUT_SUFFIX}, the expected and correct output
#       + There is only one right answer, and it should exactly match the output
#   - The tested program takes as input stdin, and outputs to stdout


# Defaults:
# RUN:                    program to be tested (project specific)
# DEFAULT_TEST_DIR:       where to search for tests
# N:                      number of processes to run by default
# AGGREGATOR:             shell function that sumarizes results
# {INPUT,OUTPUT}_SUFFIX:  Suffixes that distinguish between input and output

PAR_DIR=$(dirname `pwd`)
FILE="../main.py"
RUN="export PYTHONPATH=${PAR_DIR}; { cat ${FILE} - |  python3 -; } "
DEFAULT_TEST_DIR="./IA201819-Solitaire-Tests"
N=4
AGGREGATOR="awk_aggregator"
#AGGREGATOR="shell_aggregator"
INPUT_SUFFIX="input"
OUTPUT_SUFFIX="output"

# Alternative aggregator of results, slower but written in shell
shell_aggregator() {

  passed=0
  failed=0
  error=0
  total=0
  ret=0

  while read -r line ; do
      ret=$(printf '%b\n' "$line" | cut -b1 -)

      # increase counters
      total=$((total+1))
      if   [ "$ret" -eq 0 ]; then passed=$((passed+1));
      elif [ "$ret" -eq 1 ]; then failed=$((failed+1));
      elif [ "$ret" -eq 2 ]; then error=$((error+1));
      fi

  done

  printf '\nTotal: %b\nPassed: %b\nFailed: %b\nError in test: %b\n' \
  $total $passed $failed $error

}

# Default aggregator, shows ordered list of failed tests
awk_aggregator() {

awk '{  total++;
        if($1 == 0) files_passed[passed++] = $2; else
        if($1 == 1) files_wrong[failed++] = $2; else
        files_error[error++] = $2;
     }
     END{
         if(failed > 0) {
            asort(files_wrong)
            printf "Failed tests:\n"
            for(i in files_wrong) print files_wrong[i];
          }
          if(error > 0) {
            asort(files_error)
            printf "Errored tests:\n"
            for(i in files_error) print files_error[i];
          }
          if(passed < error + failed) {
            asort(files_passed)
            printf "Passed tests:\n"
            for(i in files_passed) print files_passed[i];
          }

            printf "\nTotal: %d\nPassed: %d\nFailed: %d\nError in test: %d\n",
            total, passed, failed, error;
        }'
}


main() {

  # Parse flags

  while getopts ":af:n:e:ih" opt; do
    case "$opt" in
      a)
        TESTS_DIR="$TESTS_DIR $DEFAULT_TEST_DIR" ;;
      f)
        TESTS_DIR="$OPTARG $TESTS_DIR" ;;
      n)
        if [ "$OPTARG" -ge 0 ] ; then
          N="$OPTARG"
        fi ;;
      e)
        RUN="$OPTARG" ;;
      i)
        DIFF_MODE="true" ;;
      h)
        printf 'USAGE: ./test.sh [OPTIONS] [TESTS AND/OR DIRS]\n'
        printf '\055a\t\t\t\tRun all the tests in the default directory\n'
        printf '\055f FILE_OR_DIR\t\t\tRun the specified test, or tests in dir\n'
        printf '\055n N\t\t\t\tNumber of tests to run concurrently (def.: 4)\n'
        printf '\055e COMMAND\t\t\tCommand to run the project\n'
        printf '\055i\t\t\t\tShow diff of each test\n'
        printf '\055h\t\t\t\tDisplay this help message\n'
        exit 0 ;;
      \?)
        echo "Use -h for help" >&2; exit 1 ;;
      :)
        printf "Option -%b requires an argument" "$OPTARG" >&2; exit 1 ;;
    esac
  done
  shift $((OPTIND - 1))


  TESTS_DIR="$* $TESTS_DIR"

  if [ ! "$TESTS_DIR" ] ; then
    TESTS_DIR="$DEFAULT_TEST_DIR"
  fi

  if [ -z "$DIFF_MODE" ]; then
    TEST="{ $RUN < \$1 | cmp -s \"\${1%$INPUT_SUFFIX}$OUTPUT_SUFFIX\" - ; printf \"%b %b\\\n\" \$? \$1; }"


    # Inspired/taught by
    # https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html
    find $TESTS_DIR -type f -name "*$INPUT_SUFFIX" -print0 | \
    xargs -0 -n1 -P"$N" sh -c "$TEST" "xarg_test" | \
    "$AGGREGATOR"

  else
    TEST="{ echo \$1 ; $RUN < \$1 | diff -s \"\${1%$INPUT_SUFFIX}$OUTPUT_SUFFIX\" - | less; }"


    # xargs was the only way I found that gave the hope of using \0-terminated
    # strings as arguments in sh POSIX
    find $TESTS_DIR -type f -name "*$INPUT_SUFFIX" -print0 | \
    xargs -0 -n1 -P1 sh -ec "$TEST" xarg_test

  fi
}

main "$@"
